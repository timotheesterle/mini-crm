$.getJSON("./data/crm.json", function(data) {
	const template = `<section class="desc">
	<img src="{{picture}}" alt="avatar">
	<div>
		<h2>{{first_name}} {{last_name}}</h2>
		<h3>{{title}}</h3>
		<p>
		{{description}}
		<ul>
			<li>Birthday : {{birthday}}</li>
			<li>E-mail : {{email}}</li>
			<li>Phone : {{phone}}</li>
			<li>Company : {{company}}</li>
		</ul>
		</p>
	</div>
</section>

<section class="info">
	<section class="notes">
		<h2>Notes</h2>
		{{#notes}}
			<h3>{{subject}}</h3>
			{{note}}<br>
			Related to : {{related_to}}
		{{/notes}}
	</section>

	<section class="tasks">
		<h2>Tasks</h2>
		<ul>
		{{#tasks}}
			<li>{{task}}</li>
		{{/tasks}}
		</ul>
	</section>
</section>

<section class="tags">
	<h2>Tags</h2>
	<ul class="tagslist">
	{{#tags}}
		<li>{{.}}</li>
	{{/tags}}
	</ul>
</section>`;
	let html = "";

	data.customers.forEach(user => (html += Mustache.to_html(template, user)));

	$("#app").html(html);
});
